import Foundation
import UIKit

class SettingsUtil {
    
    struct Storyboard {
        static let settingsCell = "SettingsCell"
        static let settingsCollectionHeader = "SettingsCollectionHeader"
        static let settingsCollectionFooter = "SettingsCollectionFooter"

    }
    
    static let settingsCollectionTitle = "قم باختيار الصفحات التي تريد ظهورها في الصفحة الرئيسيه"
}
