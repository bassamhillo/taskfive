import Foundation
import UIKit

class MainScreenUtil {
    
    struct Storyboard {
        static let mainCollectionHeader = "MainCollectionHeader"
        static let mainCollectionCell = "MainCollectionCell"
    }
    
    static let mianScreenTitle = "الصفحه الرئيسيه"
    static let mainScreenCollectionTitle = "اهلا وسهلا بك في E- Dealer"
}
