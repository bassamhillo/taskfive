//
//  SettingsCollectionViewController.swift
//  TaskFive
//
//  Created by Julia on 7/28/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit


class SettingsCollectionViewController: UICollectionViewController {
    
    @IBOutlet var settingsCollectionView: SettingsCollectionView!
        
    let dataManager = DataManager.dataManager
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let items = dataManager.getAllMenuItems()
        settingsCollectionView.setData(data: items)
        
        let footerNib = UINib(nibName: "SettingsCollectionFooter", bundle: nil)
        
        settingsCollectionView.register(footerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: SettingsUtil.Storyboard.settingsCollectionFooter)
        
        settingsCollectionView.viewController = self
    }
}
