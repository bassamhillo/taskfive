import UIKit
import SideMenu

// Navigation Controller
class MenuDrawerController: SideMenuNavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presentationStyle = .menuSlideIn
        self.menuWidth = 360

        self.navigationBar.barTintColor =  UIColor.init(red: 151/255.0, green: 203/255.0, blue: 90/255.0, alpha: 1.0)
    }
    
    



}
