
import UIKit
import FittedSheets

class MainScreenViewController: UIViewController {
    
    @IBOutlet weak var mainScreenCollectionView: MainMenuCollectionView!

    var dataManager = DataManager.dataManager
    var sheetVC:BottomSheetViewController?
    var sheetController: SheetViewController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the background color
        self.view.backgroundColor = UIColor.init(red: 151/255.0, green: 203/255.0, blue: 90/255.0, alpha: 1.0)
        
        mainScreenCollectionView.clickedDelegate = self    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let items = dataManager.getMenuItems()
        // Set the collection data
        mainScreenCollectionView.setData(items: items)
        mainScreenCollectionView.reloadData()
    }
    
    
    func initSheetVC() {
        sheetVC = self.storyboard?.instantiateViewController(identifier: "BottomSheetViewController") as? BottomSheetViewController
        
        sheetController = SheetViewController(controller: sheetVC!)
        sheetController!.topCornersRadius = 20
    }



}

extension MainScreenViewController: ItemClickedDelegate {
    func clicked(message: String) {
        initSheetVC()
        sheetVC!.labelTxt = message
        self.present(sheetController!, animated: false, completion: nil)

        
    }
    
    
}

