import UIKit

class BottomSheetViewController: UIViewController {
    
    @IBOutlet var bottomSheetView: BottomSheetView!
    var labelTxt:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bottomSheetView.mainLabel.text = labelTxt
    }
}
