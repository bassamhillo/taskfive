import Foundation
import UIKit

class DataManager {
    
    private var items: [MainMenuItem] = []
    private var isSelectedItems: [Bool]?
    static var dataManager = DataManager()
    
    func getMenuItems() ->  [MainMenuItem]{
        
        items = []
        var index = 0
        
        while index < 9 {
            let item = MainMenuItem()
            item.image = UIImage(named: "\(index + 1)")
            
            if isSelectedItems != nil {
                item.isSelected = isSelectedItems![index]
                if isSelectedItems![index] == false {
                    index += 1
                    continue
                }
            }

            switch index {
            case 0:
                item.label = "الاعدادات"
            case 1:
                item.label = "الشرائح"
            case 2:
                item.label = "الكودات المختصرة"
            case 3:
                item.label = "الصيانه"
            case 4:
                item.label = "التحصيلات"
            case 5:
                item.label = "دائرةالموزعيين"
            case 6:
                item.label = "أضف"
            case 7:
                item.label = "خدمات"
            case 8:
                item.label = "تواصل معنا"
            default:
                item.label = ""
            }
            items.append(item)
            index += 1
        }
        return items
    }
    
    func getAllMenuItems() ->  [MainMenuItem]{
        
        items = []
        var index = 0
        
        while index < 9 {
            let item = MainMenuItem()
            item.image = UIImage(named: "\(index + 1)")
            
            if isSelectedItems != nil {
                item.isSelected = isSelectedItems![index]
            }

            switch index {
            case 0:
                item.label = "الاعدادات"
            case 1:
                item.label = "الشرائح"
            case 2:
                item.label = "الكودات المختصرة"
            case 3:
                item.label = "الصيانه"
            case 4:
                item.label = "التحصيلات"
            case 5:
                item.label = "دائرةالموزعيين"
            case 6:
                item.label = "أضف"
            case 7:
                item.label = "خدمات"
            case 8:
                item.label = "تواصل معنا"
            default:
                item.label = ""
            }
            items.append(item)
            index += 1
        }
        return items
    }
    
    func setIsSelectedItem(array:[Bool]) {
        if array.count == items.count {
            isSelectedItems = array
        }
    }
}
