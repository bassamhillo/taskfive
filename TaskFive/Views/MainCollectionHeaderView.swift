//
//  MainCollectionHeaderView.swift
//  TaskFive
//
//  Created by Julia on 7/26/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class MainCollectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var mainCollectionLabel: UILabel!
    
    var mainLabelTxt:String! {
        didSet {
            mainCollectionLabel.text = mainLabelTxt
        }
    }
    
}
