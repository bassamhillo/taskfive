//
//  SettingsCollectionFooter.swift
//  TaskFive
//
//  Created by Julia on 7/28/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

protocol AddPageDelegate {
    func save()
}

class SettingsCollectionFooter: UICollectionReusableView {
    
    var addPageDelegate:AddPageDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @IBAction func addPaged(_ sender: Any) {
        addPageDelegate?.save()
    }
}
