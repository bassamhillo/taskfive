//
//  SettingsCollectionCell.swift
//  TaskFive
//
//  Created by Julia on 7/28/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

protocol CheckBoxClikedDelegate {
    func valueChanged(sender: Int,value: Bool)
}

class SettingsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    
    var itemIndex: Int?
    
    var CheckBoxDelegate:CheckBoxClikedDelegate?
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        CheckBoxDelegate?.valueChanged(sender: itemIndex!, value: sender.isSelected)
    }
}
