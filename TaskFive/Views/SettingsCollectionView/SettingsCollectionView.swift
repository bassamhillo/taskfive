//
//  SettingsCollectionView.swift
//  TaskFive
//
//  Created by Julia on 7/28/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class SettingsCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var items:[MainMenuItem]?
    var collectionViewFlowLayout:UICollectionViewFlowLayout!
    var selectedItem:[Bool] = []
    var viewController:UIViewController?
    
    func setData(data:[MainMenuItem]) {
        self.items = data
        self.setItemSize()

        self.dataSource = self
        self.delegate = self
    }

    private func setItemSize() {
        if collectionViewFlowLayout == nil {
            let numberOfItemPerRow: CGFloat = 3
            let lineSpacing: CGFloat = 10
            let interSpacing: CGFloat = 0
            
            let width = (self.frame.width - 74 - ((numberOfItemPerRow-1) * interSpacing))
                / numberOfItemPerRow
            let height = width + 30
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interSpacing
            collectionViewFlowLayout.headerReferenceSize = CGSize(width: self.frame.width, height: 100)
            
            self.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
    
    // MARK: - Data Source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellID = SettingsUtil.Storyboard.settingsCell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! SettingsCollectionCell
        
        let item = items![indexPath.row]
        
        selectedItem.append(item.isSelected)
    
        if item.isSelected {
            cell.checkBox.isSelected = true
        }
        
        cell.itemImage.image = item.image
        cell.itemLabel.text = item.label
        cell.itemIndex = indexPath.row
        
        cell.CheckBoxDelegate = self
        
        return cell
    }
    
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            if kind == UICollectionView.elementKindSectionHeader {
                let settingsHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SettingsUtil.Storyboard.settingsCollectionHeader, for: indexPath) as! SettingsCollectionHeader
                settingsHeader.titleLabel.text = SettingsUtil.settingsCollectionTitle
                return settingsHeader
            }
            let settingsFooter = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SettingsUtil.Storyboard.settingsCollectionFooter, for: indexPath) as! SettingsCollectionFooter
            settingsFooter.addPageDelegate = self
            
            return settingsFooter

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: 375, height: 100)
    }

}

extension SettingsCollectionView: CheckBoxClikedDelegate, AddPageDelegate {
    func save() {
        DataManager.dataManager.setIsSelectedItem(array: self.selectedItem)
        viewController?.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func valueChanged(sender: Int,value: Bool) {
        items![sender].isSelected = value
        self.selectedItem[sender] = value
    }
}
