//
//  SettingsCollectionHeader.swift
//  TaskFive
//
//  Created by Julia on 7/28/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class SettingsCollectionHeader: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    
}
