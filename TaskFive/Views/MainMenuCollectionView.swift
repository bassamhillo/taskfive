//
//  MainMenuCollectionView.swift
//  TaskFive
//
//  Created by Julia on 7/26/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit
import FittedSheets

protocol ItemClickedDelegate {
    func clicked(message: String)
}

class MainMenuCollectionView: UICollectionView {
    
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    var items:[MainMenuItem]?
    var clickedDelegate:ItemClickedDelegate?

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 25
        self.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
    }
    
    func setData(items:[MainMenuItem]) {
        self.dataSource = self
        self.delegate = self
        self.setItemSize()
        self.items = items
    }
    
    private func setItemSize() {
        if collectionViewFlowLayout == nil {
            let numberOfItemPerRow: CGFloat = 3
            let lineSpacing: CGFloat = 10
            let interSpacing: CGFloat = 0
            
            let width = (self.frame.width - 74 - ((numberOfItemPerRow-1) * interSpacing))
                / numberOfItemPerRow
            let height = width + 30
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interSpacing
            collectionViewFlowLayout.headerReferenceSize = CGSize(width: self.frame.width, height: 100)
            
            self.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
}

extension MainMenuCollectionView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainScreenUtil.Storyboard.mainCollectionCell, for: indexPath) as! MainCollectionViewCell
        
        let item = items![indexPath.row]
        
        cell.cellImageValue = item.image
        cell.cellLabelTxt = item.label
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: MainScreenUtil.Storyboard.mainCollectionHeader, for: indexPath) as! MainCollectionHeaderView
        
        sectionHeader.mainLabelTxt = MainScreenUtil.mainScreenCollectionTitle
        
        return sectionHeader
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let message = items![indexPath.row].label
        clickedDelegate?.clicked(message: message!)
    }
    

}


