//
//  MainScreenNavigationBar.swift
//  TaskFive
//
//  Created by Julia on 7/26/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class MainScreenNavigationBar: UINavigationBar {

    override func draw(_ rect: CGRect) {
        
        // Change the title
        self.topItem?.title = MainScreenUtil.mianScreenTitle
        // Change the title color
        self.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        // Navigation bar right Logo
        var logo = UIImage(named: "group-2")
        logo = logo?.withRenderingMode(.alwaysOriginal)
        self.topItem?.rightBarButtonItem = UIBarButtonItem(image: logo,  style: .plain, target: nil, action: nil)
        
        // Navigation bar left buttons
      /*  let menuBtn = UIBarButtonItem(image: .add, style: .plain, target: nil, action: nil)
        let notificationBtn = UIBarButtonItem(image: .add, style: .plain, target: nil, action: nil)
        
        menuBtn.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        notificationBtn.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        self.topItem?.leftBarButtonItems = [menuBtn, notificationBtn]
*/
        
    }

}
